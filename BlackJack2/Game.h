#ifndef __GAME_H
#define __GAME_H_
#include <iostream>
#include <string>

using namespace std;
class Game
{
	int UpperSelection[6];
	bool UsedUpper[6];
	int LowerSelection[6];
	bool UsedLower[6];
	int Yahtzee = 0;
	int total = 0;

	int playingP;


	//Bools for each type of lower selection
	bool ThreeOfAKind;
	bool FourOfAKind;
	bool FullHouse;
	bool LargeStraight;
	bool SmallStraight;
	bool Chance = false;

public:
	Game();
	~Game();

	int diceTotal(int x[5]);
	int diceCounter(int x, int y[5]);

	void setUpper(int selection, int score);
	void setLower(int selection, int score);
	void setYahtzee();

	bool getUpper(int selection);
	bool getLower(int selection);
	int getLowerSelection(int selection);
	int getUpperSelection(int selection);
	int getYahtzee();

	bool GameCheck1(int x[5], int y[6]);
	void GameDoubleCheck(int NumberOfDice[6], int CurrentDice);
	bool GameUpperCheck1(int NumberOfDice[6]);

	void displayGame();

	int getPlayer();
	void setPlayer();

	int getWinner(int Player1, int Player2);
};

#endif