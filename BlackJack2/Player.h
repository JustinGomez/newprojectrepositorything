#ifndef __PLAYER_H
#define __PLAYER_H

#include "Game.h"
class Player
{
	int DICE[5];
	int changedDice[5];
	int totalDice[5];
	int re_roll;
	int new_roll;
	bool turn;
	Game game;

public:
	Player();
	~Player();

	int rollDice();


	void setDice();
	void reRoll();


	void setTotalDice();

	bool MainGame();
	void showDice();
	void showScore();


	int getTotal();
};


#endif