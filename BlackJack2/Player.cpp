#include "Player.h"
#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <locale>
Player::Player()
{
	srand(time(NULL));
}


Player::~Player()
{
}

//Rolls a Dice
int Player::rollDice()
{
	int total = (1 + rand() % (6));
	return total;
}

//Creates a set of dice 
void Player::setDice()
{

	for (int i = 0; i < 5; i++)
	{
		DICE[i] =  rollDice() ;
	}

}

//Counts the number of each dice
void Player::setTotalDice()
{
	for (int i = 0; i < 6; i++)
	{
		totalDice[i] = {game.diceCounter(i+1, DICE)};
	}
}

//Re-rolls dice
void Player::reRoll()
{
	srand(time(NULL));
	for (int i = 0; i < 5; i++)
	{
		changedDice[i] = { false };
	}


	//Re-rolling
	cout << "Input number of dice to re-roll (1 - 5)" << endl;
	cin >> re_roll;
	if (re_roll > 0 && re_roll < 7)
	{
		for (int i = 0; i < re_roll; i++)
		{
			cout << "Input which dice you would like to re-roll (1 - 5)" << endl;
			cin >> new_roll;
			if (!changedDice[new_roll - 1])
			{
				int item = rollDice();
				cout << "You rolled a " << item << endl << endl;
				DICE[new_roll - 1] = item;

				changedDice[new_roll - 1] = true;
			}
			else
			{
				cout << "You have already chosen that to re-roll. Skipping the roll." << endl;
			}
		}

	}
	else
	{
		cout << "Invalid input. No dice will be re rolled." << endl;
	}

}

bool Player::MainGame()
{
	bool turn = false;
	while (!turn)
	{
		//Upper Selection Gameplay
		turn = game.GameUpperCheck1(totalDice);

		//Lower Selection Gameplay
		if (!turn)
		{
			turn = game.GameCheck1(DICE, totalDice);
		}

		cout << "Final selection" << endl << endl;

		if (!turn)
		{

			int potato = 0;
			cout << "You must choose a placement for a '0'. If you did not chose a selection before, input a '-1'." << endl
				<< " Choose a number from 1 to 12, 1-6 being the upper selection and 7-12 being the lower." << endl;
			cin >> potato;


			if ((potato > 6 && potato < 13) && !game.getLower((potato-6)-1))
			{
				game.setLower((potato-6)-1,0);
				cout << "Setting lower selection " << potato - 1 << "Which is actually" << potato - 6 - 1 << " to " << game.getLower(potato - 1) << endl << endl;
				showScore();
				return true;
			}
			else if ((potato < 7 && potato > 0) && !game.getUpper(potato-1))
			{
				game.setUpper(potato - 1, 0);
				showScore();
				return true;
			}
			else if (potato == -1)
			{
				cout << "You are retrying the selection stage" << endl << endl;
			}
			else
			{
				cout << "You did not do a proper selection. Retrying selection stage." << endl << endl;
			}
		}

	showScore();
	}
}


	void Player::showDice()
	{
		//Shows the dice
		cout << "Your dice are: ";

		for (int i = 0; i < 5; i++)
		{
			cout << DICE[i];
		}
		cout << endl;
	}

	void Player::showScore()
{
	cout << endl << endl << "SCORE BOARD: " << endl;
	for (int i = 0; i < 6; i++)
	{
		if (game.getUpper(i))
		{
			cout << "Upper selection: " << i + 1 << "   Points: " << game.getUpperSelection(i) << endl;
		}
		else
		{
			cout << "Upper selection: " << i + 1 << " is empty" << endl;
		}
	}
	cout << endl;
	for (int i = 0; i < 6; i++)
	{
		if (game.getLower(i))
		{
			cout << "Lower selection: " << i + 1 << "   Points: " << game.getLowerSelection(i) << endl;
		}
		else
		{
			cout << "Lower selection: " << i + 1 << " is empty" << endl;
		}
	}
	cout << endl << "Number of YAHTZEE's: " << game.getYahtzee() << endl << endl;
}


	//Total Score
	int Player::getTotal()
	{
		int currentTotal = 0;
		for (int i = 0; i < 6; i++)
		{
			if (game.getUpper(i))
			{
				currentTotal += game.getUpperSelection(i);
			}
		}
		if (currentTotal >= 63)
		{
			currentTotal += 35;
		}

		for (int i = 0; i < 6; i++)
		{
			if (game.getLower(i))
			{
				currentTotal += game.getLowerSelection(i);
			}
		}

		if (game.getYahtzee() == 1)
		{
			currentTotal += 50;
		}
		else if (game.getYahtzee() > 1)
		{
			currentTotal += (50 + ((game.getYahtzee() - 1) * 100));
		}

		return currentTotal;
	}
