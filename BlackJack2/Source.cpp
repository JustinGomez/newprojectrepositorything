
/*
This is my stuff. First project. Yay (By Justin Gomez)
*/

#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <locale>
#include "Player.h"

/* Easy way to initialize an array is: 
array<int,SIZE> intArray = {}
get a point: intArray.size();
change something: intArray[X] = Y;
*/
using namespace std;



//Checks and returns the number of each dice
int Counter(int x, int y[5])
{
	int total = 0;
	for (int i = 0; i < 5; i++)
	{
		if (y[i] == x)
		{
			total += 1;
		}
	}
	cout << total << endl;
	return total;
}


//Main game loop
void main()
{
	//Creates player 1 and 2
	Player p1;
	Player p2;
	Game game;
	bool playing = true;
	string testing;
	int reRolling;

	for (int i = 0; i < 12; i++)
	{
		cout << "Turn: " << i+1 << endl;
		if (game.getPlayer() == 1)
		{
			cout << "Player 1's Turn" << endl;
			reRolling = 3;
			p1.setDice();
			p1.setTotalDice();
			p1.showDice();
			while (reRolling > 0)
			{
				cout << "Would you like to re-roll any dice?(Y)" << endl;
				cin >> testing;
				if (testing == "y" || testing == "Y")
				{
					reRolling -= 1;
					p1.reRoll();
				}
				else
				{
					cout << "Input was not Y, no dice will be rolled" << endl;
					reRolling = 0;
				}

				p1.showDice();
				p1.setTotalDice();
			}

			p1.MainGame();
			game.setPlayer();
		}
		if (game.getPlayer() == 2)
		{
			{
				cout << "Player 1's Turn" << endl;
				reRolling = 3;
				p2.setDice();
				p2.setTotalDice();
				p2.showDice();
				while (reRolling > 0)
				{
					cout << "Would you like to re-roll any dice?(Y)" << endl;
					cin >> testing;
					if (testing == "y" || testing == "Y")
					{
						reRolling -= 1;
						p2.reRoll();
					}
					else
					{
						cout << "Input was not Y, no dice will be rolled" << endl;
						reRolling = 0;
					}

					p2.showDice();
					p2.setTotalDice();
				}

				p2.MainGame();
				game.setPlayer();
			}


		}
		int winner = game.getWinner(p1.getTotal(), p2.getTotal());
		cout << "The winner is: Player" << winner <<  endl;

	}
}